## How to Install Ukrainian Language Pack

There are 3 different methods to install this language pack.

### ✓ Method #1. Composer method (Recommend)
Install the Ukrainian language pack via composer is never easier.

**Install Ukrainian pack**:

```
composer config repositories.language-uk-ua vcs http://git.winmile.top:7990/scm/magento2/language-uk_ua.git
composer require winmile/language-uk_ua:dev-master
php bin/magento setup:static-content:deploy uk_UA
php bin/magento cache:flush

```


**Update  Ukrainian pack**:

```
composer config repositories.language-uk-ua vcs http://git.winmile.top:7990/scm/magento2/language-uk_ua.git
composer update winmile/language-uk_ua:dev-master
php bin/magento setup:static-content:deploy uk_UA
php bin/magento cache:flush

```
